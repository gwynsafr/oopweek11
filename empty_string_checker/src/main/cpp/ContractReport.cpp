#include "ContractReport.h"

void ContractReport::print(std::list<Contract> contracts) {
    for (Contract contract : contracts) {
        std::cout << contract << std::endl;
    }
}