#ifndef UNTITLEDCLASSES_ONETIMESERVICE_H
#define UNTITLEDCLASSES_ONETIMESERVICE_H

#include <iostream>
#include <string>
#include <utility>

#include "ProvidedService.h"

class OnetimeService : private ProvidedService {
public:
  double ServiceCost = 0.0;
  OnetimeService(int id, std::string name, double ServiceCost,
          std::string description);

  OnetimeService();

  friend std::ostream &operator<<(std::ostream &os, const OnetimeService &service) {
    std::cout << service.getName()<< " | " << service.ServiceCost << " | " << service.getDescription() << std::endl;
  };

  const double &getServiceCost() const { return this->ServiceCost; };
  void setName(const double &newServiceCost) { this->ServiceCost = newServiceCost; };
 void recommendService();
};

#endif // UNTITLEDCLASSES_ONETIMESERVICE_H