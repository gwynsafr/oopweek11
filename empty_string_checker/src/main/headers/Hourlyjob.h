#ifndef UNTITLEDCLASSES_HOURLYJOB_H
#define UNTITLEDCLASSES_HOURLYJOB_H

#include <iostream>
#include <string>
#include <utility>

#include "Job.h"

class Hourlyjob : private Job {
private: 
  double PayRate = 0.0;
  int HoursWorked = 0;
public:
  Hourlyjob(long id, std::string name, double PayRate, int HoursWorked);
  Hourlyjob();

  friend std::ostream &operator<<(std::ostream &os,
                                  const Hourlyjob &hourlyjob) {
    std::cout << hourlyjob.getName() << " | " << hourlyjob.PayRate << std::endl;
  };

  const double &getjobSalary() const { return this->PayRate; };
  void setjobSalary(const double &newpayrate) { this->PayRate = newpayrate; };

  void setHoursWorked(const double &hoursworked) { this->HoursWorked = hoursworked; };

  double calculatePayment();
};

#endif // UNTITLEDCLASSES_HOURLYJOB_H