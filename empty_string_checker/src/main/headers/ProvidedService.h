#ifndef UNTITLEDCLASSES_ProvidedService_H
#define UNTITLEDCLASSES_ProvidedService_H

#include <iostream>
#include <string>
#include <utility>

class ProvidedService {
private:
  long id = 0;
  std::string name = "empty";
  std::string description = "empty";
public:
  ProvidedService(long id, std::string name, std::string description);
  ProvidedService();

  const std::string &getName() const { return this->name; };
  void setName(const std::string &newname) { this->name = newname; };

  const std::string &getDescription() const { return this->description; };
  void setDescription(const std::string &newdescription) {
    this->description = newdescription;
  };
  virtual void recommendService() = 0;
};

#endif // UNTITLEDCLASSES_ProvidedService_H